import { rolePage, removeRole } from '../api/role'
import { ElMessageBox } from 'element-plus'
import { TipElMessage } from '@/utils/utils'

import { ref } from '@vue/reactivity'
const useRole = () => {
	const loading = ref(false) // 加载状态
	// 分页参数, 供table使用
	const page = ref({
		index: 1,
		limit: 10,
		total: 0,
	})

	// 搜索参数
	const roleParms = ref({
		inputValue: '',
	})

	const tableData = ref([]) // 表格数据

	// 表格数据
	const fetchTableData = () => {
		getTableData(true, tableData, roleParms.inputValue, loading, page)
	}

	// 删除某项
	const removeRoleItem = (id) => {
		deleteRoleItem(id, loading, fetchTableData)
	}
	return {
		page,
		tableData,
		roleParms,
		loading,
		fetchTableData,
		removeRoleItem,
	}
}

// 获取表格数据 init 默认为false，用于判断是否需要初始化分页
const getTableData = (init, tableData, search, loading, page) => {
	loading.value = true
	if (init) {
		page.value.index = 1
	}
	let params = {
		page: page.value.index,
		limit: page.value.limit,
		roleName: search,
		enable: 'ENABLE',
	}
	rolePage(params)
		.then((res) => {
			if (Object.is(res.status, 'OK')) {
				tableData.value = res.data.items
				page.value.total = Number(res.data.total)
			}
		})
		.catch((error) => {
			tableData.value = []
			page.value.index = 1
			page.value.total = 0
		})
		.finally(() => {
			loading.value = false
		})
}

// 删除对应项
const deleteRoleItem = (id, loading, fn) => {
	ElMessageBox.confirm('确定删除此角色吗？', '删除提交', {
		confirmButtonText: '确定',
		cancelButtonText: '取消',
		type: 'warning',
	}).then(() => {
		loading.value = true
		removeRole({ roleInfoId: id })
			.then(() => {
				fn()
				TipElMessage('删除成功')
			})
			.finally(() => (loading.value = false))
	})
}

export default useRole

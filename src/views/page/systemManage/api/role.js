import request from '@/utils/request'

// 分页获取角色信息
export function rolePage(data) {
	return request({
		url: '/admin/role-info/page',
		method: 'post',
		data,
	})
}

// 获取权限信息列表
export function permissionList(data) {
	return request({
		url: '/admin/permission-info/list-by-structure',
		method: 'post',
		data,
	})
}

// 添加角色信息
export function addRole(data) {
	return request({
		url: '/admin/role-info/add',
		method: 'post',
		data,
	})
}

// 根据ID删除角色
export function removeRole(data) {
	return request({
		url: '/admin/role-info/remove-by-id',
		method: 'post',
		data,
	})
}

// 根据主键更新角色
export function updateRole(data) {
	return request({
		url: '/admin/role-info/update',
		method: 'post',
		data,
	})
}

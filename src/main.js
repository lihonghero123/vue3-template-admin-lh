import { createApp } from 'vue'

import Store from './store'
import Route from './router'
import i18n from './i18n'
import App from './App.vue'
import installElementPlus from '@/plugins/element' // 引入element-plus
import '@/styles/common.scss' // 初始化样式表
import 'virtual:svg-icons-register' // 引入vite-plugin-svg-icons
import installIcons from '@/icons' // 全局注册svg组件
import installDirective from '@/directive' // 注册全局指令

import '@/config/permission.js'

const app = createApp(App)

installIcons(app)
installElementPlus(app)
installDirective(app)

app.use(Store)
app.use(Route)
app.use(i18n)
app.mount('#app')

import { createI18n } from 'vue-i18n'
import zhLocale from './lang/zh'
import enLocale from './lang/en'
import store from '@/store'

// 定义数据源
const messages = {
	zh: {
		msg: {
			...zhLocale,
		},
	},
	en: {
		msg: {
			...enLocale,
		},
	},
}

// 返回当前lang
const i18n = createI18n({
	// Composition API 模式，则需要将其设置为false
	legacy: false,
	// 全局注入$t函数
	globalInjection: true,
	locale: store.getters['app/languagePackage'],
	messages,
})

export default i18n

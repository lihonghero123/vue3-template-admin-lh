import settings from '@/config/settings'

export default {
	login: {
		title: 'User Login',
		loginBtn: 'Login',
		usernamePlaceholder: 'Please input user name',
		passwordPlaceholder: 'Please input a password',
		usernameRule: 'Username is required',
		passwordRule: 'Password cannot be less than 6 digits',
		loginOut: 'login out',
	},
	platform: {
		title: settings.title || 'vue3-admin-template',
	},
	navBar: {
		lang: 'Globalization',
	},
	toast: {
		switchLangSuccess: 'Switch Language Success',
	},
	tagsView: {
		refresh: 'Refresh',
		closeRight: 'Close Rights',
		closeOther: 'Close Others',
	},
	theme: {
		themeColorChange: 'Theme Color Change',
		themeChange: 'Theme Change',
	},
	dialog: {
		confirmText: 'confirm',
		cancelText: 'cancel',
	},
	btn: {
		add: 'add',
		search: 'search',
		operation: 'operation',
		delete: 'delete',
		edit: 'edit',
	},
	route: {
		login: 'Login',
		home: 'Home',
		profile: 'Profile',
		system: 'System',
		addRole: 'AddRole',
		editRole: 'EditRole',
		roleManage: 'RoleManage',
		journalManage: 'JournalManage',
	},
}

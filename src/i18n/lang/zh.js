import settings from '@/config/settings'

export default {
	login: {
		title: '用户登录',
		loginBtn: '登录',
		usernamePlaceholder: '请输入用户名',
		passwordPlaceholder: '请输入密码',
		usernameRule: '用户名为必填项',
		passwordRule: '密码不能少于6位',
		loginOut: '退出登录',
	},
	platform: {
		title: settings.title || 'vue3-admin-template',
	},
	navBar: {
		lang: '国际化',
	},
	theme: {
		themeColorChange: '主题色更换',
		themeChange: '主题更换',
	},
	dialog: {
		confirmText: '确定',
		cancelText: '取消',
	},
	btn: {
		add: '新增',
		search: '搜索',
		operation: '操作',
		delete: '删除',
		edit: '编辑',
	},
	toast: {
		switchLangSuccess: '切换语言成功',
	},
	tagsView: {
		refresh: '刷新',
		closeRight: '关闭右侧',
		closeOther: '关闭其他',
	},
	route: {
		login: '登录',
		home: '首页',
		profile: '个人中心',
		system: '系统管理',
		addRole: '新增角色',
		editRole: '编辑角色',
		roleManage: '角色管理',
		journalManage: '日志管理',
	},
}

import router from '@/router'
import { routes } from '@/router'
import store from '@/store'

import settings from '@/config/settings'
import { getTitle } from '@/utils/tags'
import i18n from '@/i18n/index'

import privateRoutes from '@/router/modules/private' // 动态路由(测试数据，实际项目中可删除)

// 白名单
const whitelist = settings.whiteList
/**
 * 路由前置守卫
 */
router.beforeEach(async (to, form, next) => {
	// 路由鉴权
	// 登录过不能在进入登录页，未登录只能访问登录页
	const token = store.getters['user/token']
	if (token) {
		// 登录过
		if (to.path === '/login') {
			next({ path: '/' })
		} else {
			// 是否有权限
			const permissions = store.getters['user/permissions']
			const hasPermissions = permissions && permissions.length > 0
			if (hasPermissions) {
				// 有权限
				next()
			} else {
				// 无权限
				await store.dispatch('user/getUserPermission')
				// 获取路由列表
				// const asyncRouers = await store.dispatch('user/getRouteList')
				const asyncRouers = privateRoutes
				asyncRouers.forEach((item) => {
					routes.push(item)
					router.addRoute(item)
				})
				next({ ...to, replace: true })
			}
		}
	} else {
		// 未登录
    getTitle(to,i18n.global) // 为路由添加唯一标签
		if (whitelist.includes(to.path)) {
			next()
		} else {
			next({ path: '/login' })
		}
	}
})

import { login, userPermission, userRouterList } from '@/api/system'
import { TOKEN, USERINFO } from '@/constant'
import router from '@/router'
import { setStorage, getStorage, removeAllStorage } from '@/utils/storage'
import { setTimeStamp } from '@/utils/auth'

import md5 from 'md5'

import LayOut from '@/layout/index.vue'
const modules = import.meta.glob('../../views/page/*/*.vue')

const state = {
	token: getStorage(TOKEN) || '',
	userInfo: {},
	permissionList: [], // 权限列表
}

const getters = {
	token(state) {
		return state.token
	},
	userInfoData(state) {
		return state.userInfo.id ? state.userInfo : getStorage(USERINFO)
	},
	permissions: (state) => state.permissionList,
}

// mutations
const mutations = {
	// 保存用户token
	setToken(state, token) {
		state.token = token
		setStorage(TOKEN, token)
	},
	// 保存个人信息
	setUserInfo(state, info) {
		state.userInfo = info
		setStorage(USERINFO, info)
	},
	// 保存用户权限
	setPermissions(state, data) {
		state.permissionList = data
	},
}

// action
const actions = {
	// 登录
	userLogin({ commit }, { username, password }) {
		return new Promise((resolve, reject) => {
			login({
				account: username,
				password: md5(password),
				checkCode: '1221',
				checkKey: '1491720072562216962',
				hasCheck: true,
			})
				.then((res) => {
					const { token } = res.data
					// 获取用户信息
					commit('setUserInfo', res.data)
					commit('setToken', token)
					setTimeStamp() // 设置当前登录时间戳
					resolve(res)
				})
				.catch((err) => {
					reject(err)
				})
		})
	},
	// 获取动态路由
	getRouteList({ commit }) {
		const keysList = Object.keys(modules)
		const valuesList = Object.values(modules)
		return new Promise(async (resolve) => {
			let data = []
			const res = await userRouterList()
			data = res.data
			const routerList = assembleRoute(data, keysList, valuesList)
			// 找不到路由重定向到第一个路由列表
			routerList.push({
				path: '/:pathMatch(.*)',
				component: LayOut,
				redirect: routerList[0].path,
				showMenu: true,
			})
			resolve(routerList)
		})
	},
	// 获取用户权限
	async getUserPermission({ commit }) {
		const res = await userPermission()
		if (res.status === 'OK') {
			const permiss = res.data
			if (permiss) {
				commit('setPermissions', permiss)
				return permiss
			} else {
				return false
			}
		} else {
			return false
		}
	},
	// 退出登录
	loginout({ commit }) {
		commit('setUserInfo', {})
		commit('setToken', '')
		removeAllStorage()
		router.push({ path: '/login' })
	},
}

// 组装路由表
const assembleRoute = (data, keysList, valuesList) => {
	const router = []
	if (Array.isArray(data)) {
		data.forEach((item) => {
			const newItem = {}
			if (!item.componentPath) {
				// 父级
				newItem.component = LayOut
			} else {
				newItem.component =
					valuesList[
						keysList.indexOf(
							`../..${item.componentPath.split('@')[1]}`
						)
					]
			}
			Object.keys(item).forEach((c) => {
				if (c === 'name') newItem[c] = item[c]
				// 是否隐藏该路由
				if (c === 'isHide' && item.componentPath)
					newItem.showMenu = item[c]
				if (c === 'meta') newItem[c] = item[c]
				if (c === 'path') newItem[c] = item[c]
				if (c === 'redirect') newItem[c] = item[c]
				if (c === 'children') newItem[c] = item[c]
			})
			if (newItem.children && newItem.children.length > 0) {
				// 有子路由
				newItem.children = assembleRoute(
					item.children,
					keysList,
					valuesList
				)
			}
			router.push(newItem)
		})
		return router
	}
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations,
}

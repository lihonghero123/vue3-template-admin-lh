import axios from 'axios'
import store from '@/store'
import { TipElMessage } from '@/utils/utils'
import { checkTimeOut } from '@/utils/auth'

import settings from '@/config/settings'

const baseURL = settings.baseurl
	? settings.baseurl
	: import.meta.env.VITE_APP_BASE_API

const service = axios.create({
	baseURL,
	timeout: 30000,
})

// 请求拦截器 请求前的统一处理
service.interceptors.request.use(
	(config) => {
		// JWT鉴权
		const token = store.getters['user/token']
		if (token) {
			if (checkTimeOut()) {
				// token失效 主动退出
				store.dispatch('user/loginout')
				showError('token失效')
				return
			}
			config.headers['authorization'] = token
		}
		return config // 必须返回配置
	},
	(err) => {
		return Promise.reject(err)
	}
)

// 响应拦截器 请求后的统一处理
service.interceptors.response.use(
	(response) => {
		const res = response.data
		if (Object.is(res.status, 'OK')) {
			return res
		} else {
			showError(res)
			return Promise.reject(res)
		}
	},
	// 响应失败
	(error) => {
		const err = error.response
		if (err && err.data && err.data.code === 401) {
			// token失效 被动退出
			store.dispatch('user/loginout')
			showError('token失效')
			return
		}
		return Promise.reject(error)
	}
)

// 显示错误
const showError = (error) => {
	TipElMessage(error.msg || error.errorMsg || '服务异常', 'error')
}
export default service

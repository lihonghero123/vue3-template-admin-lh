import { watch } from 'vue'

/**
 * 语言类型发生变化时，执行回调
 * @param {*} langFun
 * @param  {...any} cbs 回调数组
 */
export const watchCheckLang = (langFun, ...cbs) => {
	watch(langFun, (langType) => cbs.forEach((cb) => cb(langType)))
}

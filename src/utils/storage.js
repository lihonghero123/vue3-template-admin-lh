/**
 * 保存数据到本地
 * @param {*} key 数据名
 * @param {*} value 数据值
 */
export const setStorage = (key, value) => {
	if (typeof value === 'object') {
		value = JSON.stringify(value)
	}
	localStorage.setItem(key, value)
}

/**
 * 获取本地缓存数据
 * @param {*} key 数据名
 * @returns
 */
export const getStorage = (key) => {
	const value = localStorage.getItem(key)
	try {
		return JSON.parse(value)
	} catch (error) {
		return value
	}
}

/**
 * 删除本地指定缓存数据
 * @param {*} key 数据名
 */
export const removeStorage = (key) => {
	localStorage.removeItem(key)
}

/**
 * 删除本地缓存的所有数据
 */
export const removeAllStorage = () => {
	localStorage.clear()
}

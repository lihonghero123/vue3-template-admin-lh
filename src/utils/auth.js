import { TIME_STAMP, TIME_STAMP_VALUE } from '@/constant'
import { getStorage, setStorage } from './storage'

/**
 * 得到设置的时间戳
 * @returns
 */
export const getTimeStamp = () => {
	return getStorage(TIME_STAMP)
}

/**
 * 设置时间戳
 */
export const setTimeStamp = () => {
	setStorage(TIME_STAMP, Date.now())
}

/**
 * 判断token是否失效
 */
export const checkTimeOut = () => {
	const nowTime = Date.now()
	const oldTime = getTimeStamp()
	console.log('nowTime,oldTime', nowTime, oldTime)
	// true时效 false未失效
	return nowTime - oldTime > TIME_STAMP_VALUE
}

const route = [
	{
		path: '/login',
		showMenu: false,
		component: () => import('@/views/system/login.vue'),
		meta: { title: 'login', isInternational: true },
	},
	{
		path: '/404',
		name: '404',
		showMenu: false,
		component: () => import('@/views/system/404.vue'),
		meta: { title: '404', isInternational: false },
	},
	{
		path: '/401',
		name: '401',
		showMenu: false,
		component: () => import('@/views/system/401.vue'),
		meta: { title: '401', isInternational: false },
	},
]

export default route

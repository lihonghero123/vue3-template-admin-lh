import layout from '@/layout/index.vue'

/**
 * showMenu: 是否显示在菜单上，true显示，false隐藏
 * isInternational: 是否国际化，true是，false否
 */
const route = [
	{
		path: '/',
		component: layout,
		redirect: '/home',
		showMenu: true,
		meta: {
			title: 'home',
			icon: '',
			isInternational: true,
		},
		children: [
			{
				path: '/home',
				name: 'home',
				showMenu: true,
				component: () => import('@/views/page/home/index.vue'),
				meta: {
					title: 'home',
					icon: '',
					isInternational: true,
				},
			},
		],
	},
	{
		path: '/system',
		name: 'system',
		redirect: '/system/roleManage',
		component: layout,
		showMenu: true,
		meta: {
			title: 'system',
			icon: '',
			isInternational: true,
		},
		children: [
			{
				path: '/system/roleManage',
				name: 'roleManage',
				component: () =>
					import('@/views/page/systemManage/RoleManage.vue'),
				showMenu: true,
				meta: {
					title: 'roleManage',
					icon: '',
					isInternational: true,
				},
			},
      // 新增角色
      {
        path: '/system/addRole',
        name: 'addRole',
        component: () => import('@/views/page/systemManage/addRole.vue'),
        showMenu: false,
        meta:{
          title: 'addRole',
          icon: '',
          isInternational: true
        }
      },
			{
				path: '/system/journalManage',
				name: 'journalManage',
				component: () =>
					import('@/views/page/systemManage/JournalManage.vue'),
				showMenu: true,
				meta: {
					title: 'journalManage',
					icon: '',
					isInternational: true,
				},
			},
			{
				path: '/system/personal',
				name: 'personal',
				component: () =>
					import('@/views/page/systemManage/personal.vue'),
				showMenu: true,
				meta: {
					title: 'profile',
					icon: '',
					isInternational: true,
				},
			},
		],
	},
]

export default route

export const generateRoutes = (routes, i18n = {}, prefixTitle = []) => {
	let res = []
	routes.forEach((route) => {
		const {
			path,
			meta,
			meta: { title, isInternational },
			children,
		} = route
		const data = {
			path: path,
			title: [...prefixTitle],
		}
		// 当前存在isInternational时，说明可以国际化

		if (
			isInternational &&
			meta &&
			title &&
			!res.some((item) => item.path === data.path)
		) {
			if (typeof i18n.t === 'function') {
				const interTitle = i18n.t(`msg.route.${route.meta.title}`)
				data.title = [...data.title, interTitle]
				res.push(data)
			}
		}
		if (children) {
			const tempRoutes = generateRoutes(route.children, i18n, data.title)
			if (tempRoutes.length >= 1) {
				res = [...res, ...tempRoutes]
			}
		}
	})
	return res
}

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
// 引入svg插件
import viteSvgIcons from 'vite-plugin-svg-icons'

// 得到对应文件的绝对路径
const pathResolve = (dir) => resolve(__dirname, '.', dir)
const alias = { '@': pathResolve('src') }

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
	mode === 'development'
		? (alias['vue-i18n'] = 'vue-i18n/dist/vue-i18n.cjs.js')
		: ''
	return {
		base: './',
		resolve: {
			alias,
		},
		server: {
			port: 3003,
			host: '0.0.0.0',
			// open: true,
			proxy: {
				'/api': '',
			},
		},
		plugins: [
			vue(),
			viteSvgIcons({
				// 需要制定缓存的图标文件夹
				iconDirs: [resolve(process.cwd(), 'src/icons/svg')],
				symbolId: 'icon-[dir]-[name]',
			}),
		],
		cssPreprocessorOptions: {
			scss: {
				additionalData: '@import "src/styles/variables.module.scss";',
			},
		},
	}
})
